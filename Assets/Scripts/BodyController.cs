﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyController : MonoBehaviour
{
    private Transform target;

    private void Start()
    {
        target = GameObject.Find("X-wing").transform;
    }

    void Update()
    {
        transform.LookAt(new Vector3(target.position.x, transform.position.y, target.position.z));
        transform.Rotate(new Vector3(0, -90, 0));
    }
}
