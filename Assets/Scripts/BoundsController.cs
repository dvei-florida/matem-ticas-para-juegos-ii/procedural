﻿using UnityEngine;

public class BoundsController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(!other.gameObject.tag.Equals("persistent"))
        {
            Destroy(other.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.tag.Equals("persistent"))
        {
            Destroy(collision.gameObject);
        }
    }
}
