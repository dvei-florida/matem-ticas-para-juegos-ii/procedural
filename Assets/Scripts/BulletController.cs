﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float lifeTime = 3f;
    float timer;
    float seconds;

    private void Update()
    {
        timer += Time.deltaTime;
        seconds = timer % 60;

        if(seconds >= lifeTime)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
