﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [Range(0, 1)]
    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    Transform target;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("X-wing").transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 desiredPosition = target.position + offset;

        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        //Vector3 smoothedRotation = Vector3.Lerp(transform.rotation.eulerAngles, target.rotation.eulerAngles, smoothSpeed);

        //transform.rotation = Quaternion.Euler(smoothedRotation);
        transform.position = smoothedPosition;

    }
}
