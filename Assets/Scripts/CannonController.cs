﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonController : MonoBehaviour
{
    private Transform target;
    public GameObject laser;
    public Transform shootPoint;
    public float timer;
    public float limit = 2f;
    AudioSource audio;
    bool virgin = true;

    private void Start()
    {
        target = GameObject.Find("X-wing").transform;
        audio = GetComponent<AudioSource>();
    }

    void Update()
    {

        timer += Time.deltaTime;

        float clamped = Mathf.Clamp(target.rotation.z, -20, 20);

        transform.LookAt(new Vector3(target.position.x, target.position.y, target.position.z));
        transform.Rotate(new Vector3(0, -90, 0));

        if (timer >= 1 && virgin)
        {
            Shoot();
            timer = 0;
            virgin = false;
        } 

        if (timer >= limit)
        {
            Shoot();
            timer = 0;        
        }
    }

    void Shoot()
    {
        GameObject shoot = Instantiate(laser, shootPoint.position, transform.rotation);
        shoot.GetComponent<Rigidbody>().velocity = (target.position - shoot.transform.position).normalized * 10f;
        audio.Play();
    }

}
