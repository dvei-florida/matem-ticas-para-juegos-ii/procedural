﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonLifeManager : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;
    MeshRenderer mesh;
    public Material matDefault;
    public Material matDamage;
    public GameObject parent;

    private void Start()
    {
        currentHealth = maxHealth;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("x-wing-laser"))
        {
            TakeDamage(13);
            Destroy(other.gameObject);
        }
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            Destroy(parent);
        }
        //audio.Play();
        //mesh.material = matDamage;
        StartCoroutine("RestoreMesh");
    }

    IEnumerator RestoreMesh()
    {
        yield return new WaitForSeconds(0.5f);
        //mesh.material = matDefault;
    }
}
