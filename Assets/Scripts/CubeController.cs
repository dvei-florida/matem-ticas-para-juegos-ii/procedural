﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour
{

    public int pequeñez = 2;
    public GameObject fire;
    public Material damageMaterial;
    public Material defaultMaterial;
    private MeshRenderer mesh;
    GameObject bullet;
    bool startAnimation;

    List<GameObject> cubes;

    private void Start()
    {
        mesh = GetComponent<MeshRenderer>();
        try
        {
            fire.SetActive(false);
        }
        catch { }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Equals("X-wing"))
        {
            other.gameObject.GetComponent<XWingController>().TakeDamage(10);
        }
        if (other.gameObject.tag.Equals("laser"))
        {
            Destroy(other.gameObject);
        }
        if (other.gameObject.tag.Equals("heavyLaser"))
        {
            bullet = other.gameObject;
            try
            {
                mesh.material = damageMaterial;
            }
            catch { }
            
            fire.SetActive(true);
            StartCoroutine("Suicide");
        }
    }

    void ToMiniVoxel(GameObject model)
    {
        cubes = new List<GameObject>();

        float divisor = pequeñez;
        int x = (int) (model.transform.localScale.x * divisor);
        int y = (int) (model.transform.localScale.y * divisor);
        int z = (int) (model.transform.localScale.z * divisor);

        gameObject.transform.position = model.transform.position;

        var pos = gameObject.transform.position - model.transform.localScale / 2 + new Vector3(0.5f / divisor, 0.5f/ divisor, 0.5f/ divisor);

        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                for (int k = 0; k < z; k++)
                {
                    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    cube.transform.position = pos + new Vector3(i/ divisor, j/ divisor, k/ divisor);
                    cube.transform.localScale /= divisor;
                    
                    cubes.Add(cube);
                }
            }
        }

        foreach (GameObject cube in cubes)
        {
            cube.AddComponent<Rigidbody>();
            cube.GetComponent<Rigidbody>().useGravity = false;
            cube.GetComponent<MeshRenderer>().material = damageMaterial;
        }

    }

    IEnumerator Suicide()
    {
        yield return new WaitForSeconds(0.25f);
        ToMiniVoxel(gameObject);
        StartCoroutine("DestroyCubes");
        gameObject.GetComponent<MeshRenderer>().enabled = false;

    }

    IEnumerator DestroyCubes()
    {
        yield return new WaitForSeconds(1f);
        foreach (GameObject cube in cubes)
        {
            Destroy(cube);
        }
        Destroy(gameObject);
    }

}
