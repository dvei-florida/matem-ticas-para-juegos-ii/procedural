﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemieController : MonoBehaviour
{
    public GameObject CannonBody;
    public GameObject CannonCannon;

    private void Start()
    {
        CannonBody.GetComponent<BodyController>().enabled = false;
        CannonCannon.GetComponent<CannonController>().enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name.Equals("X-wing"))
        {
            CannonBody.GetComponent<BodyController>().enabled = true;
            CannonCannon.GetComponent<CannonController>().enabled = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name.Equals("X-wing"))
        {
            CannonBody.GetComponent<BodyController>().enabled = false;
            CannonCannon.GetComponent<CannonController>().enabled = false;
        }
    }
}
