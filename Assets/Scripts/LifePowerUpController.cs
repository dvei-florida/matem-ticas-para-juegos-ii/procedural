﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifePowerUpController : MonoBehaviour
{

    GameObject xWing;

    // Start is called before the first frame update
    void Start()
    {
        xWing = GameObject.Find("X-wing");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Equals("X-wing"))
        {
            other.gameObject.GetComponent<XWingController>().RestoreLife();
            Destroy(gameObject);
            Debug.Log("vida a tope");
        }
        
    }
}
