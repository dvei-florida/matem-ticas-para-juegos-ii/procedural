﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public int xSize = 100;
    public int zSize = 100;
    public float zoom = 30f;
    public float velocity = 1f;
    int randomStartPosition;

    public GameObject mapPrefab;
    public GameObject cannonPrefab;
    public GameObject life;
    
    int k;
    int z;
    int iterations;  
    GameObject xWing;

    void Start()
    {
        xWing = GameObject.Find("X-wing");

        k = (int)transform.position.z;
        randomStartPosition = Random.Range(0, 100);

        InvokeRepeating("UpdateShipDistance", 0f, 0.5f);

    }

    void UpdateShipDistance()
    {

        if (xWing.transform.position.z + 150 > iterations)
        {
            for (int i = 0; i < 50; i++)
            {
                GenerateLine();
            }
        }

    }

    void GenerateLine()
    {

        for (int i = 0; i < zSize; i++)
        {
            float color = GetColor(i + randomStartPosition, z);

            if (color <= 0.4)
            {
                GameObject wall = Instantiate(mapPrefab, new Vector3(i, 0, k), Quaternion.identity);
            }

            if (color > 0.4)
            {
                float num = Random.Range(0, 100);
                float num2 = Random.Range(0, 100);
                float height = Random.Range(0, 4);

                if (num < 1)
                {
                    GameObject cannon = Instantiate(cannonPrefab, new Vector3(i, height, k), Quaternion.identity);
                }

                if (num == num2 && num > 50 && num2 > 50)
                {
                    GameObject powerUp = Instantiate(life, new Vector3(i, -0.95f, k), Quaternion.identity);
                    powerUp.transform.Rotate(new Vector3(0, 90, 0));
                }
            }

        }

        z++;
        k++;
        iterations++;

    }

    float GetColor(int i, int j)
    {
        float x = (float)i / xSize * zoom;
        float z = (float)j / zSize * zoom;

        return Mathf.PerlinNoise(x, z);
    }

}
