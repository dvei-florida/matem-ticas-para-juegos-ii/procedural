﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneController : MonoBehaviour
{

    Transform xWing;
    
    // Start is called before the first frame update
    void Start()
    {
        xWing = GameObject.Find("X-wing").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, xWing.position.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Equals("X-wing"))
        {
            other.gameObject.GetComponent<XWingController>().TakeDamage(10);
        }
        if (other.gameObject.tag.Equals("laser"))
        {
            Destroy(other.gameObject);
        }
    }
}
