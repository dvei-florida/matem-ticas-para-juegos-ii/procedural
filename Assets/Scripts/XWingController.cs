﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class XWingController : MonoBehaviour
{

    public float mouseSensivity = 100f;
    public float velocity = 3f;
    public int maxHealth = 100;
    public int currentHealth;
    public HealthBarController healthBar;

    float xRotation = 0f;
    float yRotation = 180f;
    float zRotation = 0f;
    CharacterController controller;
    AudioSource audio;
    MeshRenderer mesh;
    public Material matDefault;
    public Material matDamage;
    float provisionalTimer;
    Text timeText;
    public GameObject canvasOver;
    public GameObject canvas;
    public GameObject fade;
    public Text textTime;

    void Start()
    {
        currentHealth = maxHealth;
        Cursor.lockState = CursorLockMode.Locked;
        controller = GetComponent<CharacterController>();
        audio = GameObject.Find("ImpactSound").GetComponent<AudioSource>();
        mesh = GetComponent<MeshRenderer>();
        timeText = GameObject.Find("Time").GetComponent<Text>();
    }

    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensivity * Time.deltaTime;
        float shipRotation = Input.GetAxis("Ship Rotation");


        xRotation += mouseY;
        yRotation += mouseX;
        zRotation += shipRotation;

        float clampedX = Mathf.Clamp(xRotation, -90, 90);
        float clampedY = Mathf.Clamp(yRotation, 90, 270);
        float fixedZRotation = Mathf.Lerp(transform.rotation.eulerAngles.z, shipRotation,Time.deltaTime);

        transform.localRotation = Quaternion.Euler(clampedX, clampedY, zRotation);
        //transform.Rotate(Vector3.left, zRotation);

        controller.Move(-transform.forward * velocity * Time.deltaTime);
        Timer();
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
        if (currentHealth <= 0)
        {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            canvasOver.SetActive(true);
            canvas.SetActive(false);
            fade.SetActive(true);
        }
        audio.Play();
        mesh.material = matDamage;
        StartCoroutine("RestoreMesh");
    }

    public void RestoreLife()
    {
        currentHealth = maxHealth;
        healthBar.SetHealth(currentHealth);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.gameObject.tag.Equals("x-wing-laser"))
        {
            TakeDamage(10);
        }
    }

    IEnumerator RestoreMesh()
    {
        yield return new WaitForSeconds(0.5f);
        mesh.material = matDefault;
    }

    void Timer()
    {
        provisionalTimer += Time.deltaTime;

        string minutes = Mathf.Floor(provisionalTimer / 60).ToString("00");
        string seconds = (provisionalTimer % 60).ToString("00");

        string count = string.Format("{0}:{1}", minutes, seconds);

        timeText.text = "Mission time: " + count;
        textTime.text = "Game Over!! Time survived: " + count;
    }

    public void ReloadScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("SampleScene");
    }
}
