﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XWingShoot : MonoBehaviour
{
    public List<GameObject> firepoints;
    public GameObject laser;
    public float cooldown;
    public float timer;
    public float heavyTimer;
    public float heavyCooldown;
    int position;
    AudioSource audioBlaster;
    bool virgin = true;
    bool heavyVirgin = true;
    public int heavyAmmo = 3;
    public GameObject heavyLaser;
    public GameObject heavyFirepoint;
    public GameObject uiAmmo1;
    public GameObject uiAmmo2;
    public GameObject uiAmmo3;
    float timerHeavyRestore;
    float seconds;

    private void Start()
    {
        audioBlaster = GetComponent<AudioSource>();
    }

    void Update()
    {
        timer += Time.deltaTime;
        heavyTimer += Time.deltaTime;

        timerHeavyRestore += Time.deltaTime;
        seconds = timerHeavyRestore % 60;

        if (seconds >= 60)
        {
            heavyAmmo++;
            HeavyAmmoManager();
            timerHeavyRestore = 0;
        }

        if (Input.GetAxis("Fire1") > 0 && timer >= cooldown)
        {
            audioBlaster.Play();
            StartCoroutine("Shoot"); 
            timer = 0;
        }

        if (Input.GetAxis("Fire2") > 0 && heavyAmmo > 0 && heavyTimer >= heavyCooldown)
        {
            StartCoroutine("HeavyShoot");
            heavyTimer = 0;
        }
    }

    IEnumerator Shoot()
    {     
        for (int i = 0; i<4; i++)
        {
            if (virgin)
            {
                SpawnLaser();
                virgin = false;
            }
            else
            {
                yield return new WaitForSeconds(0.25f);
                SpawnLaser();
            }
        }                   
    }

    IEnumerator HeavyShoot()
    {
        if (heavyVirgin)
        {
            SpawnHeavyLaser();
            heavyVirgin = false;
        }
        else
        {
            yield return new WaitForSeconds(0.25f);
            SpawnHeavyLaser();
        }
    }

    void SpawnLaser()
    {
        position = position > 3 ? 0 : position;
        GameObject firepoint = firepoints[position];
        GameObject shoot = Instantiate(laser, firepoint.transform.position, firepoint.transform.rotation);
        shoot.GetComponent<Rigidbody>().velocity = (-transform.forward).normalized * 20f;
        position++;
    }

    void SpawnHeavyLaser()
    {
        GameObject shoot = Instantiate(heavyLaser, heavyFirepoint.transform.position, heavyFirepoint.transform.rotation);
        shoot.GetComponent<Rigidbody>().velocity = (-transform.forward).normalized * 10f;
        int aux = heavyAmmo;
        aux--;
        heavyAmmo = Mathf.Clamp(aux, 0, 3);
        HeavyAmmoManager();
    }

    void HeavyAmmoManager()
    {
        switch (heavyAmmo)
        {
            case 1: uiAmmo1.SetActive(true); uiAmmo2.SetActive(false); uiAmmo3.SetActive(false); break;
            case 2: uiAmmo1.SetActive(true); uiAmmo2.SetActive(true); uiAmmo3.SetActive(false); break;
            case 3: uiAmmo1.SetActive(true); uiAmmo2.SetActive(true); uiAmmo3.SetActive(true); break;
            default: uiAmmo1.SetActive(false); uiAmmo2.SetActive(false); uiAmmo3.SetActive(false); break;
        }
    }
}
