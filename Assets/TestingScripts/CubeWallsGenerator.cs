﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeWallsGenerator : MonoBehaviour
{
    
    public GameObject model;

    int x;
    int y;
    int z;

    List<GameObject> cubes;

    private void Start()
    {
        
        cubes = new List<GameObject>();

        x = (int) model.transform.localScale.x;
        y = (int) model.transform.localScale.y;
        z = (int) model.transform.localScale.z;

        gameObject.transform.position = model.transform.position;

        var pos = gameObject.transform.position - model.transform.localScale / 2 + new Vector3(0.5f, 0.5f, 0.5f);

        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                for (int k = 0; k < z; k++)
                {
                    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    cube.transform.parent = gameObject.transform; 
                    cube.transform.position = pos + new Vector3(i, j, k);
                    cubes.Add(cube);
                }
            }
        }
    }
}


