﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralGenerator : MonoBehaviour
{
    public GameObject wallPrefab;
    public float xSize;
    public float zSize;
    public float zoom;

    int randomStartPosition;
    int z;
    

    // Start is called before the first frame update
    void Start()
    {
        randomStartPosition = Random.Range(0, 100);
        GenerateMap();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void GenerateMap()
    {
        for (int x = 0; x < xSize; x++)
        {
            float color = GetColor(x + randomStartPosition, z);

            if (color <= 0.4)
            {
                //GameObject wall = Instantiate(wallPrefab, new Vector3(x * wallPrefab.GetComponent<CubeWallsGenerator>().x, 0, transform.position.z), Quaternion.identity);
            }

        }
    }

    float GetColor(int i, int k)
    {
        float x = (float)i / xSize * zoom;
        float z = (float)k / zSize * zoom;

        return Mathf.PerlinNoise(x, z);
    }


}
